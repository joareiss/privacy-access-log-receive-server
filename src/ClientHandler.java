import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

class ClientHandler implements Runnable {

    private Socket socket;

    public ClientHandler(Socket s) {
        socket = s;
    }

    public void run() {
        int filesize = 1024 * 10; //10mb..
        int bytesRead;
        int currentTot = 0;

        try {
            System.out.println("Getting new file...");
            byte[] byteArray = new byte[filesize];
            InputStream is = socket.getInputStream();

            Date now = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_hh_mm");
            FileOutputStream fos = new FileOutputStream("receivedLogs" + sdf.format(now) + ".csv");

            BufferedOutputStream bos = new BufferedOutputStream(fos);
            bytesRead = is.read(byteArray, 0, byteArray.length);
            currentTot = bytesRead;

            while (bytesRead > 0){
                bytesRead = is.read(byteArray, currentTot, (byteArray.length - currentTot));
                if (bytesRead >= 0) currentTot += bytesRead;
            }

            bos.write(byteArray, 0, currentTot);
            bos.flush();
            bos.close();
            socket.close();
            System.out.println("DONE");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}